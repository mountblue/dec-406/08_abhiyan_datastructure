class Node {
    constructor(value , left = null , right = null){
        this.value = value;
        this.left = left;
        this.right=right;
    }
}
module.exports = class BinarySearchTree {
    //Constructor for the Binary Tree
    constructor(){
        this.root = null ;
        this.answer = [];
    }
    searchTree(value,node=this.root){
        if(node.value == value){
          return true;
        }
        if(value<node.value && node.left)
         return this.searchTree(value,node.left);
        
         if(value>node.value && node.right)
          return this.searchTree(value,node.right);
        
          return false;
    }
    addNodeRecursively(value,node=this.root){
        if(node == null) {
            node = new Node(value);
            return node;
        }
        if(value<node.value) {
          node.left = this.addNodeRecursively(value,node.left);    
        }
        if(value>node.value) {
          node.right = this.addNodeRecursively(value,node.right);  
        }
       return node;
    }
    addNode(value) {
        if(this.root == null){
           this.root = new Node(value);
           return;
        }
        let current = this.root;

        while(1) {
            if(value<current.value){
                if(current.left == null){
                  current.left = new Node(value);
                  break;
                }
                current = current.left;
            }
            else if(value>current.value){
                
                if(current.right == null){
                 current.right = new Node(value);
                 break;
                }
                current=current.right;
                 
            }
        }
    return;
    }

    //Inorder Traversal
    inorderTraversal(node = this.root) {
     if(node.left)
      this.inorderTraversal(node.left);
     this.answer.push(node.value);
    
     if(node.right)
      this.inorderTraversal(node.right);
    }

   deleteNode(value,node=this.root){
       if(value == node.value){
        if(node.left==null && node.right==null) {
            return null;
        } 
        else if(node.left==null ){
             return node.right;
           }
           else if(node.right==null){
             return node.left;
           }
            else {
            let currentNode = node.right;
            while(currentNode.right!=null){
              currentNode=currentNode.right;  
            }
            this.deleteNode(currentNode.value);
            node.value = currentNode.value;
            
            return node;
           }
       }
     if(value<node.value){
         node.left = this.deleteNode(value,node.left);
     }
     if(value>node.value){
         node.right = this.deleteNode(value,node.right);
     }
     return node;
    
   }
}

