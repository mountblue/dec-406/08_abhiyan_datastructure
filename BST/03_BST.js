/*
Name:03_BST.js
Description:Merge two binary trees
Input:Two BST's
Output:Merges list of nodes in sorted order
Author:abhiyan timilsina
*/
var BinarySearchTree = require('./BinarySearchTree.js');

const mergeTrees = (tree1= bt1 , tree2=bt2)=>{
   tree1.inorderTraversal();
   tree2.inorderTraversal();
   return [...tree1.answer , ...tree2.answer].sort((a,b)=>a-b);
}
var bt1 = new BinarySearchTree();
var bt2 = new BinarySearchTree();

bt1.addNode(10);
bt1.addNode(8);
bt1.addNode(12);
bt1.addNode(6);
bt1.addNode(11);
bt1.addNode(5);


bt2.addNode(20);
bt2.addNode(18);
bt2.addNode(23);
bt2.addNode(16);
bt2.addNode(25);
bt2.addNode(15);

console.log(mergeTrees());