/*
Name:04_BST.js
Description:TO check whether the tree is BST
Input:A BST
Output:Whether BST or not
Author:abhiyan timilsina
*/
var BinarySearchTree = require('./BinarySearchTree.js');

var checkForBST = function(node = bin.root){
   if(node.left){
       if(node.left.value<node.value)
        return checkForBST(node.left);
       else
        return false;
   }
   if(node.right){
       if(node.right.value>node.value) {
           return checkForBST(node.right);
       }
       else{
           return false;
       }
   }
   return true;
}

var bin = new BinarySearchTree();
bin.addNode(10);
bin.addNode(8);
bin.addNode(12);
bin.addNode(6);
bin.addNode(11);
bin.addNode(5);
bin.deleteNode(5);
console.log(checkForBST());