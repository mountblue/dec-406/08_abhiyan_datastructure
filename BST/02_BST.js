/*
Name:02_BST.js
Description:Delete nodes from BST
Input:A BST
Output:BST with the given node deleted
Author:abhiyan timilsina
*/
var BinarySearchTree = require('./BinarySearchTree.js');


var bt1 = new BinarySearchTree();
bt1.addNode(10);
bt1.addNode(8);
bt1.addNode(12);
bt1.addNode(6);
bt1.addNode(11);
bt1.addNode(5);
bt1.addNode(9);

bt1.deleteNode(10);
bt1.inorderTraversal();
