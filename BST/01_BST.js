/*
Name:01_BST.js
Description:Find whether the tree is BST of not
Input:A BST with root
Output:The path from source to destination.
Author:abhiyan timilsina
*/

var BinarySearchTree = require('./BinarySearchTree.js');


var isBST = (node)=>{
    if(node.left){
        if(node.left.value<node.value)
         return isBST(node.left);
        else
         return false;
    }
    if(node.right){
        if(node.right.value>node.value) {
            return isBST(node.right);
        }
        else{
            return false;
        }
    }
    return true;         
}
let length = 0;
var findLargestBST = (node=bt1.root)=>{
  
  if(isBST(node)){
     bt1.inorderTraversal(node);
     
     if(length<bt1.answer.length)
     length = bt1.answer.length;

     bt1.answer = [];
      return true;
  }
  else {
    if(node.left){
        return findLargestBST(node.left);
    }
    if(node.right){
        return findLargestBST(node.right);
    }
  }
  return false;
}

var bt1 = new BinarySearchTree();
bt1.addNode(10);

bt1.addNode(8);
//Change to find 
bt1.root.left.value=100;
bt1.addNode(12);
bt1.addNode(6);
bt1.addNode(11);
bt1.addNode(5);

findLargestBST();
console.log(length);
let count  = bt1.inorderTraversal();