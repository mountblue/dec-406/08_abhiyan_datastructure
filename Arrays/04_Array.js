/*                                    
*Name:04_Array.js
*Description:To find print the array in pendulum form
*Input:An array of integers  
*Output : pendulum form
*author:abhiyan timilsina
*/

//F.E to return the elements
const printPendulum = (array)=>{
    let newArr = new Array(array.length);
   
        let elem = array.splice(0,1)[0];
        if(newArr.length%2 == 0){
            newArr[Math.floor((newArr.length-1)/2)] = elem;
        }
        else {
            newArr[Math.floor(newArr.length/2)] = elem;
        }
        for(let i =0 ;i<array.length ; i++) {
            if(i%2==0) {
                newArr.push(array[i])
            }
            else {
                newArr.unshift(array[i]);
            }
        }
         array=[];
     newArr.forEach((ele)=>{
        if(ele) {
            array.push(ele);
        }
    });
    return array;
}


//Test Case 1
console.log(printPendulum([1,2,5,4].sort((a,b)=>a-b)));

//Test Case 2
console.log(printPendulum([10,2,15,14,12,13,11,4].sort((a,b)=>a-b)));
