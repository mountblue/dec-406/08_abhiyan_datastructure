/*                                    
*Name:02_Array.js
*Description:To find the product under given Modulo
*Input:An array of integers and a number 
*Output : Product under modulo
*author:abhiyan timilsina
*/

//F.E to return the result
const productUnderModulo = (arr,modulo)=>arr.reduce((accumulator,value)=>{
   return accumulator * value;
},1)%modulo;

//Test Case 1
console.log(productUnderModulo([1,2,3,4,5],123));

//Test Case 2
console.log(productUnderModulo([122,234,355,466,577],200));


