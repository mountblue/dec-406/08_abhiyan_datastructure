/*                                    
*Name:03_Array.js
*Description:To find the missing number in set of sequential array
*Input:An array of integers  
*Output : Missing integer
*author:abhiyan timilsina
*/

//F.E to return the missing integers
var findKthMissing = (array,k)=>{
  let missing= []
  for(let i = 0 ; i<array.length ; i++) {
    if(array[i+1]!=array[i]+1 && i!=array.length-1) {
      missing.push(array[i]+1);
    }
  }
  
  return missing.length==0?-1:missing;
}

//Test Case 1
console.log(findKthMissing([1,2,3,4,5,6,7],3));

//Test Case 2

console.log(findKthMissing([10,13,15,16,17],3));