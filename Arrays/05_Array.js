/*                                    
*Name:05_Array.js
*Description:To find print the closest
*Input:Two arrays of integers  
*Output : closest integers
*author:abhiyan timilsina
*/

//F.E to return the elements
const printClosest = (array1,array2 ,x)=>{
    let distances = [array1[0],array2[0]] ;
    let minDistance = array1[0]+array2[0];
    for(let i = 0 ; i<array1.length ; i++) {
        for(let j = 0 ;j<array2.length ; j++) {
             if(array1[i]+array2[j]<=x && array1[i]+array2[j]>minDistance){
                 minDistance = array1[i]+array2[j];
                 distances= [array1[i],array2[j]];
             }
        }
    }
  return distances;
}

//Test Case 1
console.log(printClosest([1,2,3,4,5],[1,2,3,4,5],9));

//Test Case 2
console.log(printClosest([1,4,,5,7],[10,20,30,40],32));