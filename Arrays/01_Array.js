/*                                    
*Name:01_Array.js
*Description:To find the sum of all the array elements except the current element
*Input:An array of integers
*Output : An array of sums 
*author:abhiyan timilsina
*/

//Function expression for returning the anser
const sumOfArray = arr=>new Array(arr.length)
                            .fill(0)
                            .map((elem,index)=>{
                              let ar = arr;
                              let sum = 0;
                              for(let i = 0 ;i<ar.length;i++) {
                                 if(i!=index)
                                   sum+=ar[i];
                              }
                           return sum;
                            });

//Test Case 1:                            
console.log(sumOfArray([1,2,3,4,5,6]));


//Test Case 2:
console.log(sumOfArray([10,20,33,44,56,67]));
