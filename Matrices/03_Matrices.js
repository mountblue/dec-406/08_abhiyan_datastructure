/*
Name:03_Matrices.js
Description:To find wheather train detaches or not
Input:n*n grid represented as array
Output:true if train doesn't detaches else false
Author:abhiyan timilsina
*/

//function to check the condition
const validateTrack = (row,column,pathString)=>{
   let path =  pathString.split('');
   let r =row;
   let c =column;
   let gridCells = new Array(row).fill(0).map((value,index)=>index+1)
      .map((value,index)=>{
       let gridRow = [];
      for(let k = 0 ; k<column ; k++) {
         gridRow.push([value,k+1]);
       }
      return gridRow;
   });
 loop1:
  for(let row=0 ; row<gridCells.length ; row++) {
      for(let column = 0 ; column<gridCells[row].length ; column++) {
          
          let gridRowNo = gridCells[row][column][0];
          let gridColNo = gridCells[row][column][1];
          for(let i = 0 ; i<path.length ; i++) {
            if(path[i]=='L'){
               gridRowNo = gridRowNo-1; 
            }
            else if(path[i]=='R') {
                gridRowNo = gridRowNo+1;
            }
            else if(path[i]=='U') {
                gridColNo = gridColNo-1;
            }
             else if(path[i]=='D') {
                gridColNo = gridColNo+1;
            }
     
        }
        if((gridRowNo>0&&gridRowNo<=r) && (gridRowNo>0&&gridRowNo<=c)){
            return 1
        }  
      }
 }
 return 0;
}
console.log(validateTrack(1,1,'R'));