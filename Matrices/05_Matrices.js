/*
Name:05_Matrices.js
Description:To print the matrix in spiral form
Input:n*n grid represented as array
Output:Matrix printed in spiral form
Author:abhiyan timilsina
*/

//Function to print matrix in spiral form
const printSpiral = (matrix)=>{
    let rowLength = matrix.length;
    let columnLength = matrix[0].length;
     let i = 0;
     let j = columnLength-1;
    while(i<rowLength) {
        
        for(let k=i ; k<columnLength ; k++) {
            console.log(matrix[i][k]);
        }
        i++;
        for(let k = i; k<rowLength ; k++){
            console.log(matrix[k][j]);
        }
        i = j ;
        j--;
        
    }
}