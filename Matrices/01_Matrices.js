/*
Name:01_Matrices.js
Description:To find wheather path detaches or not
Input:n*n grid
Output:true if it doesnt detach else false
Author:abhiyan timilsina
*/

//Global variables
let visited = [];
let rowSize;
let colSize;
let traveseMatrix;
let found = false;
var findPath = (matrix)=>{
   
    traveseMatrix = matrix;
    let sourcePos = [];
     rowSize = matrix.length;
     colSize = matrix[0].length;

    //Finding the source element position
    for(let i = 0 ; i<matrix.length ; i++) {
        if(~matrix[i].indexOf(1)) {
           sourcePos = [i,matrix[i].indexOf(1)];
        }
    }
    visited.push(String(sourcePos[0]+sourcePos[1]));
    return findConnected(sourcePos);    
}

var findConnected= (source)=>{
    
    if(visited.length == traveseMatrix.length*traveseMatrix[0].length){
        return;
    }
    if(visited.indexOf(String(source[0]+source[1]) == -1)){
        visited.push(String(source[0]+source[1]));
    }
   //UP DOWN LEFT RIGHT
    let neighbours = [[source[0],source[1]-1],[source[0],source[1]+1],[source[0]-1,source[1]],[source[0]+1 , source[1]]];
    neighbours.forEach((neighbour,index)=>{
        if((neighbour[0]<0 || neighbour[0]>=rowSize) || (neighbour[1]<0 || neighbour[1]>=colSize)){
          neighbours.splice(index,1);
        }
    });
    if(neighbours.filter((neighbour)=>traveseMatrix[neighbour[0]][neighbour[1]]==2).length){
        console.log("PATH EXIST");
        found = true;
        return;
    }

   neighbours =  neighbours.filter((neighbour)=>traveseMatrix[neighbour[0]][neighbour[1]] !== 0);
    
   for(let i = 0 ; i <neighbours.length ; i++) {
       if(visited.indexOf(String(neighbours[i][0]+neighbours[i][1]))==-1){
           findConnected(neighbours[i]);
       }
   }
    
}
//Test Case 1
findPath([[0,3,1,0],[3,0,3,3],[2,3,0,2],[0,3,3,3],]);
console.log(found);
//Test Case 2
findPath([[0,3,2],[3,0,0],[1,0,0]]);
console.log(found);