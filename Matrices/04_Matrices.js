/*
Name:04_Matrices.js
Description:To find rotation of matrix
Input:n*n grid represented as array
Output:Rotated matrix
Author:abhiyan timilsina
*/


const rotateMatrix = (matrix)=>{
    matrix = matrix.map((row)=>row.reverse());
    let mat2 = [];
    for(let i = 0 ; i<matrix.length ; i++) {
         let row = [];
        for(let j = 0 ; j <matrix[i].length ; j++){
            row.push(matrix[j][i]);
        }
        mat2.push(row);
    }
    return mat2;
   
}
console.log(rotateMatrix([[1,2,3],[4,5,6],[7,8,9]]));