/*
Name:02_LinkedList.js
Description:Find the number of times the elemnt occurs
Input:A linkedlist with elements
Output:Return the number of occurances
Author:abhiyan timilsina
*/

var LinkedList = require('./linkedList.js');
const countGiven = (listItems , number)=>{
    var list = new LinkedList();

    for(let i = 0 ; i<listItems.length ; i++)
       list.addNewNode(listItems[i]);
    
       console.log(list.printList());
       let pos = 0;
       let count = 0;
       
      while(pos<list.length){
          
          if(list.searchNode(number,pos)!=-1) {
              count++;
          }
          else {
              break;
          }
          pos = list.searchNode(number,pos)+1;
      }
     console.log(count);

      
}

countGiven([1,2,3,4,5,3,1,2,3,1,1,1,1,1],1);