/*
Name:01_LinkedList.js
Description:Arrange list in odd and even
Input:A linkedlist with elements
Output:A linked list in ordered form
Author:abhiyan timilsina
*/


var LinkedList = require('./linkedList.js');

const arrangeList = (listItems)=>{
    var list = new LinkedList();
    for(let i = 0 ;i<listItems.length ; i++) {
      list.addNewNode(listItems[i]);
    }
    
    var evenElement = list.printList().filter(value=>value%2==0);
    
    for(let i=0 ; i<evenElement.length ;i++) {
        list.deleteNode(list.searchNode(evenElement[i]));
        list.addNewNode(evenElement[i],i);
    }
   console.log(list.printList());
}
arrangeList([1,2,3,4,5,6]);

