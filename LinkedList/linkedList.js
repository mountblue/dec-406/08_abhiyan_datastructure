class Node {
    constructor(value,next) {
        this.value = value;
        this.next = next;
    }
}
module.exports =  class LinkedList {
    constructor() {
        this.head = null;
        this.length = 0;
    }
   
    addNewNode(value,position=-82974) {
        if(this.head == null){
            this.head  = new Node(value,null);
        }
        else if(position==-82974){
            let cur = this.head;
            while(cur.next != null) {
              cur = cur.next;    
            }
            cur.next = new Node(value,null);
        }
        else {
            if(position<0 || position>this.length){
                throw new Error("Please give corrent range");
            }
            
          if(position==0) {
              let newHead = new Node(value,this.head);
              this.head = newHead;
            }
            else if(position == this.length){
                this.addNewNode(value,-82974);
            }
            else {
            let nodeCount = 0 ;
            let prev = this.head;
            let cur = this.head;
            while(nodeCount!=position) {
                prev = cur;
                cur = cur.next;
                nodeCount++;
            }
            let newNode = new Node(value,cur);
            prev.next = newNode;
            } 
        }
        this.length++;
    }
   //Method to print the list
    printList(){
        let cur = this.head;
        let list = [];
        do {
          list.push(cur.value);    
         cur=cur.next;
        }while(cur!=null);
        return list;
  }

  //Deleting a Node at given position
  deleteNode(position) {
      if(position>this.length-1 || position<0){
          throw new Error('Position must be in given range');
      } 
     let nodeCount = 0 ;
      let cur = this.head;
      let prev = this.head;
      let deletedValue;
      while(nodeCount != position) {
          prev = cur;
          cur = cur.next;
          nodeCount++;
      } 
      if(cur == this.head){
        let newHead = this.head.next;
        deletedValue = this.head.value;
        delete this.head;
        this.head = newHead;
      }
      deletedValue = cur.value;
      prev.next = cur.next;
      return deletedValue; 
    }
   
    //Search a value with given offset
    searchNode(value,offset=0){
        let nodeCount = 0;
        let cur = this.head;
        
        //Throws an error for invalid offsets
        if(offset>this.length-1 || offset<0) {
          throw new Error(`The offset must be within the range of linkedlist`);       
        }

        //Positioning in the required offset
        while(cur!=null){
            if(nodeCount>=offset && cur.value == value) {
                return nodeCount;
            }
            cur = cur.next
           nodeCount++;
        }
        return -1; 
    }
}

