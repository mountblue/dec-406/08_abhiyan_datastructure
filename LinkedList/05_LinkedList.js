/*
Name:04_LinkedList.js
Description:Remove duplicates
Input:A linkedlist with elements
Output:Print the modified  list
Author:abhiyan timilsina
*/

var LinkedList = require('./linkedList.js');

var linkedList = new LinkedList();
var items = [1,1,2,3,3,4,4,4,5,5,5,6,7,8,8,8,9,9,9,10];
items.forEach((item)=>{
     linkedList.addNewNode(item);
 });
 removeDuplicates();
console.log(linkedList.printList());
 function removeDuplicates() {
     let currentNode = linkedList.head;
     let position = 0;
     while(currentNode.next != null){
         if(currentNode.value == currentNode.next.value){
             let nodePointer = currentNode.next;
             while(currentNode.value == nodePointer.value){
                 linkedList.deleteNode(position+1);
                 nodePointer = nodePointer.next;
             }
         }
         else{
             currentNode = currentNode.next;
             position++;
         }
    }
 }

