/*
Name:04_LinkedList.js
Description:Find the sorted linked list
Input:A linkedlist with elements
Output:Print the sorted list
Author:abhiyan timilsina
*/

var LinkedList = require('./linkedList.js');

var sortedList = (listItems)=>{
    var list = new LinkedList();
    for(let i = 0 ;i<listItems.length ; i++) {
      list.addNewNode(listItems[i]);
    }
    
    var sortedElement = list.printList().sort((a,b)=>a-b);
    
    for(let i=0 ; i<sortedElement.length ;i++) {
        list.deleteNode(list.searchNode(sortedElement[i]));
        list.addNewNode(sortedElement[i],i);
    }
   console.log(list.printList());
}

sortedList([1,2,10,4,5,6]);