/*
Name:03_LinkedList.js
Description:perform pop operation on list
Input:A linkedlist with elements
Output:linked list with deleted node
Author:abhiyan timilsina
*/

var LinkedList = require('./linkedList.js');

var popList = ()=>{
   let list = new LinkedList();
   list.addNewNode(10);
   list.addNewNode(12);
   list.addNewNode(13);
   list.addNewNode(14);
   console.log(list.deleteNode(0));
}

popList();