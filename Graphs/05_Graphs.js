/*
Name:05_Graphs.js
Description:Find wheather graph is cyclic or not
Input:A graph having nodes and edges
Output:Whether graph is cyclic or not
Author:abhiyan timilsina
*/
var Graph = require('./Graph.js');

var findCyclic = (numberOfVertices , noOfEdges , inputString)=>{
    var graph = new Graph(numberOfVertices,noOfEdges);
    inputString = inputString.split('').filter((element)=>element!=' ');
    while(inputString.length){
        let edge = inputString.splice(0,2);
        graph.addToGraph(Number(edge[0]),Number(edge[1]));
    }
  //Checking for self cycles
  for(let j = 0 ; j<graph.adjacencyMatrix.length ; j++){
    if(graph.adjacencyMatrix[j][j]==1){
        return true;
    }
  }

  
  let visited = [];
  return detectCycle(0);
  function detectCycle(source){
     if(~ visited.indexOf(source)) {
        return true;
    }
    visited.push(source);
    //Finding adjacent edges to the source
    for(let j=0 ;j<graph.adjacencyMatrix[source].length;j++){
        if(graph.adjacencyMatrix[source][j]==1){
            return detectCycle(j);
        }
    }
    return false;
  }

  
}

console.log(findCyclic(2,2,'0 0 1 0'));


console.log(findCyclic(4,3,'0 1 1 2 2 3'));