var LinkedList = require('./linkedList.js');

module.exports = class Graph {
    constructor(vertices,edges) {
        this.verticesCount = vertices;
        this.edges = edges;
        this.adjacencyMatrix = new Array(vertices);
        this.connectedNodes = [];
        for(let i = 0 ;i<this.verticesCount ;i++){
            for(let j=0 ; j<this.verticesCount ; j++){
                if(this.adjacencyMatrix[i]==undefined){
                    this.adjacencyMatrix[i] = new Array(this.verticesCount);
                }
                this.adjacencyMatrix[i][j]=0;
            }
        }
        
        this.adjacencyList = new Array(vertices);
    }
    addToGraph(source , destination=null) {
        if(this.connectedNodes.indexOf(source)==-1){
            this.connectedNodes.push(source)
        }
        if(this.connectedNodes.indexOf(destination)==-1 && destination!=null)
          this.connectedNodes.push(destination);

        this.addToAdjacencyMatrix(source,destination);
        this.addToAdjacencyList(source,destination);

    }
    addToAdjacencyMatrix(source,destination){
       if(destination===null || destination<0 || destination>this.verticesCount-1){
         console.log('....',source,destination,this.verticesCount);  
        throw new Error('Invalid destination');
       }
          
       this.adjacencyMatrix[source][destination] = 1;
       return true;
    }

    addToAdjacencyList(source,destination){
      let linkedList = this.adjacencyList[source]; 
      if(linkedList==undefined){
           linkedList = new LinkedList();
           this.adjacencyList[source] = linkedList;
        }
       if(destination != null)
       linkedList.addNewNode(destination)
       return true;
    }
    printAdjacencyList(){
        for(let i = 0 ; i<this.adjacencyList.length ;i++){
            if(this.adjacencyList[i]==undefined){
                console.log(`${i}--->empty List \n`);
                continue;
            }
            let list = this.adjacencyList[i];
            list = list.printList();
            console.log(`${i}--->${list}\n`);
        }
    }
    deleteFromGraph(node){
        if(node<0 || node>this.verticesCount-1){
         console.log(node);
            throw new Error('Invalid node number');
        }
         
        this.adjacencyMatrix[node] = new Array(this.verticesCount).fill(0);

        for(let i=0;i<this.verticesCount ; i++){
            if(i==node)
             continue;
            this.adjacencyMatrix[i][node]=0;
        }
        
    }
    
    dfsTraversal(source){
        
      if(source<0 || source>this.verticesCount-1){
          throw new Error('Invalid Source');
      }
      let visited = [];
      
      var  dfs = (source)=>{
          
         if(!visited.includes(source))
          visited.push(source);
         else {
             return;
         }
         
         let connectedNeighbours = [];
         this.adjacencyMatrix[source].forEach((value,index)=>{
            if(value==1){
                connectedNeighbours.push(index);
            }
         });
         connectedNeighbours.forEach((node)=>{
             dfs(node);
         });
      }

      dfs(source);
      return visited;
    }
  
}