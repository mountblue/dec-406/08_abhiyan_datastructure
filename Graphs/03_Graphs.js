/*
Name:02_Graphs.js
Description:Count the number of X's
Input:A graph having nodes and edges
Output:Count of X's
Author:abhiyan timilsina
*/

var Graph = require('./Graph.js');

const findNumberOfShapes = (inputArray)=>{
  let graph = new Graph(inputArray.length , inputArray[0].length);
  
  for(let i = 0 ; i<inputArray.length ; i++) {
      for(let j= 0 ; j<inputArray[i].length ; j++){
          if(inputArray[i][j]=='X') {
              graph.addToGraph(i,j);
          }
        }
  }
  let count = 0;
  for(let i = 0 ; i<graph.adjacencyMatrix.length ; i++){
      for(let j = 0 ; j<graph.adjacencyMatrix[i].length ; j++){
         if(graph.adjacencyMatrix[i][j] == 1){
          count++;
          for(k=i+1;k<graph.verticesCount;k++){
              if(graph.adjacencyMatrix[k][j]==1)
               {
                   count++;
               }
               graph.deleteFromGraph(k)
          }
          graph.deleteFromGraph(i);
          break;
         }
      }
  }
  console.log(count);
}

// findNumberOfShapes([['X','X','X'],['O','O','O'],['X','X','X']]);
findNumberOfShapes([['X','X','X'],['X','O','O'],['X','O','O']]);