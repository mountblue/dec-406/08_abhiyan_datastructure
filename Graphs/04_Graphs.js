/*
Name:04_Graphs.js
Description:Find even distances 
Input:A graph having nodes and edges
Output:The node of even distances.
Author:abhiyan timilsina
*/
var Graph = require('./Graph.js');

var findEvenDistance = (vertices , inputString)=>{
    var graph = new Graph(vertices,null);
    inputString = inputString.split('').filter((element)=>element!=' ');
    while(inputString.length){
        let edge = inputString.splice(0,2);
        graph.addToGraph(Number(edge[0]-1),Number(edge[1]-1));
        graph.addToGraph(Number(edge[1]-1),Number(edge[0]-1));
    }
    var distaceMatrix = new Array(vertices);
    for(let i = 0 ;i<distaceMatrix.length;i++) {
         
        distaceMatrix[i] = new Array(vertices).fill(999);
        distaceMatrix[i][i]=0;
        for(let k = 0 ; k<vertices ; k++){
            if(graph.adjacencyMatrix[i][k]==1){
                distaceMatrix[i][k]=1;
            }
        }
    }
    count = 0;
    for(let k=0 ; k<vertices ; k++){
        source = k;
        for(let i=0 ; i<distaceMatrix.length ;i++) {
            for(let j = 0 ; j<distaceMatrix.length ;j++){
             if(distaceMatrix[source][i]>distaceMatrix[source][j]+distaceMatrix[j][i]){
                 distaceMatrix[source][i]=distaceMatrix[source][j]+distaceMatrix[j][i];
                 if(distaceMatrix[source][i]%2==0 && distaceMatrix[source][i]!=0)
                  count++;
             }
            }
            
        }
    } 

    return count;
}

console.log(findEvenDistance(3,'1 2 2 3'));