/*
Name:02_Graphs.js
Description:Find path between rooms
Input:A graph having nodes and edges
Output:The path from source to destination.
Author:abhiyan timilsina
*/
var Graph = require('./Graph.js');
let path = [];
const findPath = (source=0)=>{
    
   
    if(!path.includes(source)) {
         path.push(source);
    }
    if(path.length==matrix.length && path[matrix.length-1]==matrix.length-1){
        return;
    }
    
    let paths = graph.adjacencyMatrix[source].map((value,index)=>{
        
        if(value==1 && !path.includes(index)){
            
            return index;
        }
        else {
            return -2;
        }
    }).filter(value=>value!==-2);


  
    if(paths.length==0){
        
        path.pop();
        return ;
    }
    else{
        
        paths.forEach((val)=>findPath(val));
    }
    
}

let matrix = [[0,-1,1,-1,-1],[1,0,1,1,-1],[1,1,0,1,-1],[1,1,1,0,1],[-1,-1,-1,1,0]];
var graph = new Graph(matrix.length,2);

graph.adjacencyMatrix = matrix;
findPath();
console.log(path);