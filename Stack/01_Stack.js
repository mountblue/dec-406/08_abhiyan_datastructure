/*
*Name: 01_Stack.js
*Description: Delete the middle element of the stack
*Input: A stack of numbers represented as array
*Output: A stack of numbers with middle number deleted
*author:abhiyan timilsina
*/

//Function to return the elements of stack
const deleteMiddle = (stack)=>{
  let stack2 = [];
  let count = 0;
  while(true){
      let elem = stack.pop();
      if(elem==undefined)
        break;
      count++;
      stack2.push(elem);
  }
  var mid = count/2;
  while(mid>0){
      let elem = stack2.pop();
      mid--;
      stack.push(elem);
  }
  stack2.pop();
  while(true){
      let elem = stack2.pop();
      if(elem == undefined)
      break;
      stack.push(elem);
  }
  return stack;
}
//Test case 1
var stack = [];
stack.push(1,2,3,4,5,6,7,8);
stack = deleteMiddle(stack);
console.log(stack);