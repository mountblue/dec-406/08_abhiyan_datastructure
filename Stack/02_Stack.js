/*
*Name: 02_Stack.js
*Description: Parse brackets
*Input: A string of brackets
*Output: the number of brackets
*author:abhiyan timilsina
*/

//Function to parse given brackets
var parseBracket = (expression)=>{
 let stack = [];
 
 expression =expression.split("");
 
 for(let i = 0 ; i<expression.length ; i++){
     if(expression[i] ==')' ){
        
        while(1) {
             elem = stack.pop();

             if(elem == '('){
                 break;
             }
             else if(!isNaN(elem)){
                 sum+=Number(elem);
             }
        }
          stack.push(sum);
     }
     else {
        stack.push(expression[i]);
     }
     
 }
return stack[0];
 
}
console.log(parseBracket('((()))'));

