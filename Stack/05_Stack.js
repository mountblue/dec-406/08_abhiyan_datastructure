/*
*Name: 05_Stack.js
*Description: Find the nearst greater element
*Input: A stack of numbers represented as array
*Output: Nearest element which is greater
*author:abhiyan timilsina
*/
var findNearestGreater = (array) =>{
    let stack = [];

    array.forEach((element)=>stack.push(element));
    
    for(let i=array.length-1 ;i>=0 ; i--) {
      
        let stack1  = [...stack];
        while(1) {
            let elem = stack1.pop();
            if(elem == undefined){
              break;
            }
            if(elem>array[i]){
             break;
             }
        }
        stack.pop();
    }
 }
 findNearestGreater([2,10,3,5,10,1].reverse());