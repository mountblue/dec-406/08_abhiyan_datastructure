/*
*Name: 03_Stack.js
*Description: Find the nearst greater element
*Input: A stack of numbers represented as array
*Output: Nearest element which is greater
*author:abhiyan timilsina
*/
var findAbsDifference = (array) =>{
    let stack = [...array];
    
    let stack1 = [...array.reverse()];
    array.reverse();
         
   for(let i = 0 ; i<array.length ; i++){
        
       let smallestRight = 0;
       let smallestLeft = 0;
       let element = stack1.pop();
       let stack2 = [...stack1];
       while(1){
           let popElement = stack2.pop();
           if(popElement==undefined)
           break;
           if(popElement<element){
               smallestRight = popElement;
               break;
           }
         }
        
        element = array[i];
        let stack3  = [...stack];
        let found = false;
        while(1) {
            let popElement = stack3.pop();
            if(popElement == undefined){
                break;
            }
           if(popElement == array[i]){
               found = true;
           }
           if(found && popElement<array[i]){
               smallestLeft= popElement;
               break;
           }
        }
   console.log(array[i],'element:AbsDifference', Math.abs(smallestLeft-smallestRight));
     
   }
 }
 findAbsDifference([2,10,3,5,8,1]);