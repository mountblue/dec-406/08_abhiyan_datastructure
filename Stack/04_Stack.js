/*
*Name: 04_Stack.js
*Description: Find the nearst greater element
*Input: A stack of numbers represented as array
*Output: Nearest element which is smaller
*author:abhiyan timilsina
*/
var findNearestSmaller = (array) =>{
   let stack = [];
   array.forEach((element)=>stack.push(element));
   
   for(let i=array.length-1 ;i>=0 ; i--) {
      
       let stack1  = [...stack];
       while(1) {
           let elem = stack1.pop();
           if(elem == undefined){
              console.log(`There is no smaller element for ${array[i]}`); 
              break;
           }
           if(elem<array[i]){
               console.log(`The smaller element to left of ${array[i]} is ${elem}`);
               break;
            }
       }
       stack.pop();
   }
}
findNearestSmaller([2,10,3,5,10,1]);